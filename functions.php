<?php
	function showHelp(){
		print "------------------Available Options------------------\n";
		print "get-access\n";
		print "put-access <access-code>\n";
		print "whoami\n";
		print "friends\n";
		print "mywall\n";			//Incomplete
		print "status <message>\n";
		print "post-to <user-handle> <message>\n";
		print "comment-on <post-id> <message>\n";
		print "like-post <post-id>\n";		//Incomplete
		print "read-post <post-id>\n";		//Incomplete
		print "stalk <user-handle>\n";
		print "read-albums [<user-handle>]\n";
		print "read-album <album-id> [-d]\n";
		print "upload-to <album-id> <absolute-path-of-image> [<message>]\n";
		print "read-messages\n";
	}
	function checkErrors($result){
//		if($result==NULL)	die("Connection Timed Out.\n");
		if(isset($result->error)){
			if(isset($result->error->message) && strstr($result->error->message,"Session has expired"))
				die("Session has expired. Relogin required.\n");
			die("Invalid Access token... Login Failed!!!\n");
		}
		return 0;
	}
	function getSavedAccessToken(){
		$file_handle=fopen(_ACCESS,'r');
		$res=fread($file_handle,filesize(_ACCESS)); 
		fclose($file_handle);
		if(!$res)	die("Access-token Missing. Perform a 'fbcli get-access' first.\n");
		return $res;
	}
	function getSavedUserId(){
		$file_handle=fopen(_USER,'r');
		$res=fread($file_handle,filesize(_USER)); 
		fclose($file_handle);
		if(!$res)	die("User-Id file Missing\n");
		return $res;
	}
	function getUserId($name,$access_token){
		$res=readGraph(_FRIENDS,$access_token);
		$base="http://www.facebook.com/";
		foreach($res->data as $tmpobj){
			if($tmpobj->link==$base.$name)
				return $tmpobj->id;
		}
		return "me";
	}
	function readGraph($url,$access_token){
		$ch=curl_init($url.$access_token);
		curl_setopt($ch,CURLOPT_HEADER,false);
		curl_setopt($ch,CURLOPT_TIMEOUT,10);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
		return json_decode(curl_exec($ch));
	}
	function publishStatus($url,$access_token,$status){
		$postvars="message=$status";
		$ch=curl_init($url.$access_token);
		curl_setopt($ch,CURLOPT_HEADER,false);
		curl_setopt($ch,CURLOPT_TIMEOUT,10);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
		
		return json_decode(curl_exec($ch));
	}
	function publishComment($message,$url){
		$postvars="message=$message";
		$ch=curl_init($url);
		curl_setopt($ch,CURLOPT_HEADER,false);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
		curl_setopt($ch,CURLOPT_TIMEOUT,10);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
		
		return json_decode(curl_exec($ch));
	}
	function doLikePost($url,$accesstoken){
		$ch=curl_init($url);
		curl_setopt($ch,CURLOPT_HEADER,false);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
	//	curl_setopt($ch,CURLOPT_TIMEOUT,10);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_POSTFIELDS,"");
		
		return json_decode(curl_exec($ch));
	}
	function printWHOAMI($result){
		print "ID\t\t".$result->id;
		print "\nName\t\t".$result->name;
		print "\nLink\t\t".$result->link;
		print "\nProfile Pic\t".$result->picture->data->url."\n";
	}
	function printStalk($result){
		print "ID\t\t".$result->id;
		print "\nName\t\t".$result->name;
		print "\nLink\t\t".$result->link;
		print "\nProfile Pic\t".$result->picture->data->url;
		print "\nBirthday\t".$result->birthday;
		print "\nGender\t\t".$result->gender;
		print "\nHometown\t".$result->hometown->name."\n";
	}
	function printMywall($res){
	 	foreach($res->data as $tmpobj){
	 		//Incomplete Feature
	      		print "From: ".$tmpobj->from->name."\n";
	      		print "Message:\n".$tmpobj->id."\n";
	      		print "-----------------------------------------------------\n";
	      	}
	}
	function getStalkUrl($user_id){
		return "https://graph.facebook.com/".$user_id."?fields=id,name,link,hometown,birthday,gender,picture&access_token=";
	}
	function parsePhotos($res){
		foreach($res->data as $tmpobj){
			print "ID\t\t ==> ".$tmpobj->id;
			print "\nFrom\t\t ==> ".$tmpobj->from->name;
			print "\nAlbum\t\t ==> ".$tmpobj->name."\n";
			print "-----------------------------------\n";
		}
	}
	function savePhotosFromAlbum($res){
		foreach($res->data as $tmpobj){
			print "Downloading Image ".$tmpobj->id."\n";
			exec("wget -q ".$tmpobj->source);
			print "Success!\n";
		}
	}
	function fetchPhotosFromAlbum($res){
		foreach($res->data as $tmpobj){
			print "ID\t\t ==> ".$tmpobj->id;
			print "\nFrom\t\t ==> ".$tmpobj->from->name;
			print "\nURL\t\t ==> ".$tmpobj->source;
			print "\n-----------------------------------\n";
		}
		if(isset($res->paging)){
			print $res->paging->next;
		}
	}
	function printMessages($res,$threadcount,$limit){
		foreach($res->data as $thread){
			print "Thread ID \t==> $thread->id\n";
			print "Receipients\t==>";
			foreach($thread->to->data as $senders){
				print $senders->name.",";
			}
			print "\nConversation:\n";
			$l=$limit;
			foreach($thread->comments->data as $message){
				$l--;
				if(!$l)	break;
				print $message->from->name."\t\t:\t".$message->message."\n";
			}
			$threadcount--;
			if(!$threadcount)	return;
			print "------------------------------------------------------------------------\n";
		}
	}
	function uploadPic($url,$postvars){
		$ch = curl_init($url);
		curl_setopt($ch,CURLOPT_HEADER,0);
		curl_setopt($ch,CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $postvars);
		return json_decode(curl_exec($ch));
	}
	function printPost($res){
		
	}
