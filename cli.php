<?php
error_reporting('E_NONE');
	require("sdk/facebook.php");
	require("sdk/facebook_desktop.php");
	require("sdk/constants.php");
	require("functions.php");
	
	if(count($argv)==1){
		showHelp();
	}
	else if(count($argv)>=2){
		switch($argv[1]){
			case "get-access":
				exec("gnome-open "._APP_URL);
				break;
			case "put-access":
				if(!isset($argv[2]))
					die("Access-token Missing!!\n");
				$access_token=$argv[2];
			     //cURL read to test the access token
			     	print "Testing Access Token...\n";
				$res=readGraph(_TEST,$access_token);
			     //Throw Error if any
				checkErrors($res);
				echo "Success! Saving Tokens...\n";
			     //Save Access Token
				exec("touch "._ACCESS);
				exec("touch "._USER);
				$file_handle=fopen(_ACCESS,'w');
				fwrite($file_handle,$access_token);
				fclose($file_handle);
				
				$file_handle=fopen(_USER,'w');
				fwrite($file_handle,$res->id);
				fclose($file_handle);
				print "All tokens saved.\n";
				break;
			case "whoami":
			      //Fetch Access token
				$access_token=getSavedAccessToken();
				$res=readGraph(_WHOAMI,$access_token);
				checkErrors($res);
				printWHOAMI($res);
				break;
			case "stalk":
				$access_token=getSavedAccessToken();
				if(!isset($argv[2]))	die("Insert Profile handle\n");
				$url=getStalkUrl($argv[2]);
				$res=readGraph($url,$access_token);
				checkErrors($res);
				printStalk($res);
				break;
			case "friends":
			      //Get Friends' link and name
				$access_token=getSavedAccessToken();
				$user="me";
				if(isset($argv[2]))	$user=$argv[2];
				$url="https://graph.facebook.com/$user/friends?fields=link,name,id&access_token=";
				echo $url.$access_token."\n";
				$res=readGraph($url,$access_token);
				checkErrors($res);
				if(!isset($res->data))	die("Failed to retrieve...\n");
				foreach($res->data as $tmpobj){
					print $tmpobj->name."\t\t\t\t".$tmpobj->link."\n";
				}
				break;
			case "mywall":
			      //Read my wall
			      	$access_token=getSavedAccessToken();
			      	$res=readGraph(_MY_WALL,$access_token);
			      	checkErrors($res);
			      	if(!isset($res->data))	die("Failed to retrieve...\n");
			      	printMywall($res);
				break;
			case "status":
			      //Write on wall- self
				if(!isset($argv[2]))	die("Insert some message\n");
				$access_token=getSavedAccessToken();
				$user_id=getSavedUserId();
				$res=publishStatus(_POST_STATUS,$access_token,$argv[2]);
				checkErrors($res);
				echo "Post ID ==> $res->id\n";
				break;
			case "post-to":
			      //Write on someone else's wall
				if(!isset($argv[2]))	die("Insert Receipient\n");
				if(!isset($argv[3]))	die("Insert Message\n");
				$access_token=getSavedAccessToken();
				$user_id=$argv[2];
				$url="https://graph.facebook.com/".$user_id."/feed&access_token=";
				$res=publishStatus($url,$access_token,$argv[3]);
				checkErrors($res);
				echo "Post ID ==> $res->id\n";
				break;
			case "comment-on":
			      //Comment on a particular post-id
				if(!isset($argv[2]))	die("Insert Post ID\n");
				if(!isset($argv[3]))	die("Insert Message\n");
				
				$access_token=getSavedAccessToken();
				$post_id=$argv[2];
				$message=$argv[3];
				$url="https://graph.facebook.com/$post_id/comments&access_token=$access_token";
				$res=publishComment($message,$url);
				checkErrors($res);
				echo "Comment ID ==> $res->id\n";
				break;
			case "like-post":
			      //Like a given post ID
				if(!isset($argv[2]))	die("Insert Post ID\n");
				$access_token=getSavedAccessToken();
				$post_id=$argv[2];
				$url="https://graph.facebook.com/$post_id/likes&access_token=$access_token";
				echo "$url\n";
				$res=doLikePost($url);
				var_dump($res);
				checkErrors($res);
				if($res)
				echo "Post Liked\n";
				else echo "Some Error Occurred!\n";
				break;
			case "read-post":
			      //Incomplete
				if(!isset($argv[2]))	die("Insert Post ID\n");
				$access_token=getSavedAccessToken();
				$post_id=$argv[2];
				$url="https://graph.facebook.com/$post_id/?access_token=$access_token";
				$res=readGraph($url,$access_token);
				var_dump($res);
				checkErrors($res);
				printPost($res);
				break;
			case "read-albums":
			      //Read the specified user's album
				if(!isset($argv[2]))	$argv[2]="me";
				$access_token=getSavedAccessToken();
				$user_id=$argv[2];
				$url="https://graph.facebook.com/$user_id/albums?access_token=";
				$res=readGraph($url,$access_token);
				checkErrors($res);
				parsePhotos($res);
				break;
			case "read-album":
			      //Read|Download all pics from the specifed album
				if(!isset($argv[2]))	die("Specify Album Id\n");
				$access_token=getSavedAccessToken();
				$album_id=$argv[2];
				$url="https://graph.facebook.com/$album_id/photos?access_token=";
				$res=readGraph($url,$access_token);
				checkErrors($res);
				if(isset($argv[3]) and $argv[3]=='-d')	savePhotosFromAlbum($res);
				else					fetchPhotosFromAlbum($res);
				break;
			case "upload-to":
			      //Upload a specified pic to the specified album. Additional parameter <message>
				if(!isset($argv[2]))	die("Specify Album Id\n");
				if(!isset($argv[3]))	die("Specify Image to Upload\n");
				$access_token=getSavedAccessToken();
				$album_id=$argv[2];
				$image_id=$argv[3];
				$f=fopen($image_id,'r');
				if(!$f)	die("Unable to open file.\n");
				
				$message=$argv[4] or "";
				$url="https://graph.facebook.com/$album_id/photos?access_token=$access_token";
				$postvars['source']="@$image_id";
				$postvars['message']=$message;
				print "Uploading File.\n";
				$res=uploadPic($url,$postvars);
				checkErrors($res);
				print "Upload Successful.\n";
				print "Photo ID\t==> ".$res->id."\n";
				print "Post ID\t\t==> ".$res->post_id."\n";
				break;
			case "read-messages":
			      //Read the most recent messages(threads)
				$access_token=getSavedAccessToken();
				$res=readGraph(_READ_MAIL,$access_token);
				checkErrors($res);
				if(!isset($argv[2])) $argv[2]=10000;
				if(!isset($argv[3])) $argv[3]=10000;
				printMessages($res,$argv[2],$argv[3]);
				break;
			case "logout":
			      //Clear all tokens
				exec("rm -f "._ACCESS."; rm -f "._USER."; ");
				print "All tokens deleted. User successfully logged out.\n";
				break;
		}
	}
?>
